'use strict';


// Declare app level module which depends on filters, and services
angular.module('myApp', [
	'ngRoute',
	'myApp.filters',
	'myApp.services',
	'myApp.directives',
	'myApp.controllers',
	'UserApp',
    'angular-loading-bar',
    'ngAnimate',
    'ui.bootstrap',
    'googlechart'
]).
config(['$routeProvider', function($routeProvider) {
	// $routeProvider.when('/signup', {templateUrl: 'partials/signup.html', public: true}); 
	$routeProvider.when('/verify-email', {templateUrl: 'partials/verify-email.html', verify_email: true});
	$routeProvider.when('/reset-password', {templateUrl: 'partials/reset-password.html', public: true});
	$routeProvider.when('/set-password', {templateUrl: 'partials/set-password.html', set_password: true});
	$routeProvider.when('/contracts', {templateUrl: 'partials/contracts.html', controller: 'ContractCtrl'});
	$routeProvider.when('/view2', {templateUrl: 'partials/partial2.html', controller: 'MyCtrl2'});
	$routeProvider.when('/base', {templateUrl: 'base.html', public: true});
	$routeProvider.otherwise({redirectTo: '/base'});
}])
.run(function($rootScope,$location, user) {
	// Initiate the user service with your UserApp App Id
	// https://help.userapp.io/customer/portal/articles/1322336-how-do-i-find-my-app-id-
	user.init({ appId: '53832cea89943' });


    $rootScope.$on('user.logout', function() {
		    console.log('Bye!');
		    $location.path('/base');
	});

});



'use strict';

/* Controllers */



var myApp = angular.module('myApp.controllers', ['ngResource']);

myApp.factory('Contracts', ['$resource',
	function($resource) {

		return $resource(
			'https://clearspending.p.mashape.com/v1/contracts/select/?&page=:page&daterange=:daterange&placing=5&pricerange=300000-400000&regnum=&sort=price&supplierinn=&supplierkpp=', {}, {
				load: {
					method: 'GET',
					headers: {
						'X-Mashape-Key': 'Tku5NGVvE6mshcUMa289A2XGbwpSp1HgXd8jsnez5kfswPSocu'
					},
					params: {
						page: '@page',
						daterange: '@daterange'
					},
					isArray: false
				}
				/* , method2: { ... } */
			});

	}
]);

myApp.factory('CalendarDate', function() {
	var startdate = new Date();
	var endDate = new Date();
	return {
		getStart : function () {
			return startdate;
		},
		getEnd : function() {
			return enddate;
		},
		setStart : function (date) {
			startdate = date;
		},
		setEnd : function(date) {
			endDate = date;
		}
	};
});

myApp.controller('ContractCtrl', function($scope, $http, $filter, Contracts,CalendarDate) {

	$scope.currentPage = 1;
	$scope.maxSize = 15;
	$scope.itemsPerPage = 50;

	$scope.load = function() {
		/*приведем даты к фотрмату */
		var sDate = $filter('date')(CalendarDate.getStart(), 'dd.MM.yyyy');
		var eDate = $filter('date')(CalendarDate.getEnd(), 'dd.MM.yyyy');
		console.info('Request page ' + $scope.currentPage + ' with daterange ' + sDate + '-' + eDate);

		Contracts.load({}, {
			'page': $scope.currentPage,
			'daterange': sDate + '-' + eDate
		}, function(result) {

			if (angular.isUndefined(result.contracts)) {
				$scope.message = "Данные не найдены"
				$scope.error = 'true';
				return;
			}

			$scope.error = 'false';

			$scope.contracts = result.contracts;
			console.info(result);
			$scope.totalItems = $scope.contracts.total;
			$scope.numPages = $scope.contracts.total / 50 + 1;

		});

	}

	$scope.pageChanged = function() {
		console.info('Page changed to: ' + $scope.currentPage);
		$scope.load();
	};



	$scope.getIndex = function(index) {
		return index + 1 + ($scope.currentPage - 1) * 50;
	}

});
/*контроллер для управления двумя календарями */
myApp.controller('CalendarCtrl', function($scope,CalendarDate) {
	CalendarDate.setStart($scope.startdate);
	CalendarDate.setEnd($scope.enddate);

	$scope.error = 'false';

	// Disable weekend selection
	$scope.disabled = function(date, mode) {
		return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
	};

	$scope.toggleMin = function() {
		$scope.minDate = $scope.minDate ? null : new Date();
	};
	$scope.toggleMin();

	$scope.openStart = function($event) {
		$event.preventDefault();
		$event.stopPropagation();

		$scope.openedStart = true;
	};

	$scope.openEnd = function($event) {
		$event.preventDefault();
		$event.stopPropagation();

		$scope.openedEnd = true;
	};

	$scope.dateOptions = {
		formatYear: 'yy',
		startingDay: 1
	};

	$scope.initDate = new Date('2016-15-20');
	$scope.minDate = new Date('2000-01-01');
	$scope.maxDate = new Date('2016-01-01');

});

myApp.controller('MyCtrl2', function() {

});

myApp.controller("GenericChartCtrl", function($scope, $routeParams, $interval) {
	$scope.chartObject = {};

	$scope.onions = [{
		v: "Onions"
	}, {
		v: 3
	}, ];

	$scope.chartTypes = ["BarChart", "PieChart", "ColumnChart"];

	$scope.chartObject.data = {
		"cols": [{
			id: "t",
			label: "Topping",
			type: "string"
		}, {
			id: "s",
			label: "Slices",
			type: "number"
		}],
		"rows": [{
			c: [{
				v: "Mushrooms"
			}, {
				v: 3
			}, ]
		}, {
			c: $scope.onions
		}, {
			c: [{
				v: "Olives"
			}, {
				v: 31
			}]
		}, {
			c: [{
				v: "Zucchini"
			}, {
				v: 1
			}, ]
		}, {
			c: [{
				v: "Pepperoni"
			}, {
				v: 2
			}, ]
		}]
	};

	$interval(function() {
		$scope.chartObject.type = $scope.chartTypes[Math.floor(Math.random() * $scope.chartTypes.length)];
		console.info('Change chartType to : ' + $routeParams.chartType);
	}, 5000);

	$routeParams.chartType = "BarChart"; //or PieChart or ColumnChart...
	$scope.chartObject.type = $routeParams.chartType;
	$scope.chartObject.options = {
		'title': 'How Much Pizza I Ate Last Night'
	}
});